# -*- coding: iso-8859-15 -*-
import cv2
import os
import sys
import numpy as np

from matplotlib import pyplot as plt



MIN_MATCH_COUNT = 10
FRAME_OFFSET = 12

def get_time(frame_number, fps):
	
	#if (fps > 0):	
	return (frame_number * 1.0/fps) + (FRAME_OFFSET* 1.0/fps)
	#else:
	#	return 0
		

def save_frames(video):

	
	cap = cv2.VideoCapture(video)
	cap.set(1, 0)
	ret ,frame = cap.read()
	img2 = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	#cv2.imwrite('/Users/swetzel/Desktop/logfiles/Marie/out'+str(cap.get(cv2.CAP_PROP_POS_FRAMES)+FRAME_OFFSET)+ '_'+str((cap.get(cv2.CAP_PROP_POS_FRAMES)+FRAME_OFFSET)/cap.get(cv2.CAP_PROP_FPS))+'.jpg',img2)
	
	while(ret):

		# take frame of the video
		ret ,frame = cap.read()
		img2 = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		#cv2.imwrite('/Users/swetzel/Desktop/logfiles/Marie/out'+str(cap.get(cv2.CAP_PROP_POS_FRAMES)+FRAME_OFFSET)+ '_'+str((cap.get(cv2.CAP_PROP_POS_FRAMES)+FRAME_OFFSET)/cap.get(cv2.CAP_PROP_FPS))+'.jpg',img2)
	
	
def find_frame_in_video(detector,cap, ret, frame, timestamp, gaze_x, gaze_y, img1, kp1, des1, participant):

	match_found = False
	frame_found = False

	#print ("find frame " + str(frame))
	frame_time = get_time(cap.get(cv2.CAP_PROP_POS_FRAMES),cap.get(cv2.CAP_PROP_FPS))
	
	
	if ret == True and frame_time >= timestamp:
		frame_found = True
			
	while(not frame_found):

		# take frame of the video
		ret ,frame = cap.read()
		frame_time = get_time(cap.get(cv2.CAP_PROP_POS_FRAMES), cap.get(cv2.CAP_PROP_FPS))
	
		if ret == True and frame_time >= timestamp:
			frame_found = True
			break;
			
		elif ret == False:
			return -1,-1, 0, 0
			
		
	
	
	# feature detection	
	img2 = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	kp2, des2 = detector.detectAndCompute(img2,None)
	
	FLANN_INDEX_KDTREE = 0
	index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
	search_params = dict(checks = 50)

	flann = cv2.FlannBasedMatcher(index_params, search_params)
	matches = flann.knnMatch(des1,des2,k=2)

	# store all the good matches as per Lowe's ratio test.
	good = []
	
	for m,n in matches:
		if m.distance < 0.7*n.distance:
			good.append(m)
			
		
	M = None
		
	if len(good)>MIN_MATCH_COUNT:
		
		src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
		dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
		
		M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
		matchesMask = mask.ravel().tolist()

		if M is not None:
			h,w = img1.shape
			pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
		
			dst = cv2.perspectiveTransform(pts,M)
		
			img2 = cv2.polylines(img2,[np.int32(dst)],True,255,3, cv2.LINE_AA)
			match_found = True
		else:
			print ("Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT))
			matchesMask = None
			match_found = False
			
	else:
		print ("Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT))
		matchesMask = None
		match_found = False
		
	draw_params = dict(matchColor = (0,255,0), # draw matches in green color
						   singlePointColor = None,
						   matchesMask = matchesMask, # draw only inliers
						   flags = 2)	
		
	mapped_x = -1 
	mapped_y = -1
		
	if match_found == True:	
		
		if np.linalg.cond(M) < 1/sys.float_info.epsilon:
			# use inverse matrix of homography to map gaze points to reference image
			invertedM = np.linalg.inv(M)				   	   
			gaze = np.float32([ [gaze_x,gaze_y]]).reshape(-1,1,2)				   
			transformed_gaze = cv2.perspectiveTransform(gaze,invertedM)				   
		
			img2 = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)
			img2 = cv2.circle(img2,(transformed_gaze[0,0,0],transformed_gaze[0,0,1]), 12, (0,0,255), -1)
			img2 = cv2.circle(img2,(gaze_x+2048,gaze_y), 6, (0,0,255), -1)
			mapped_x = transformed_gaze[0,0,0]
			mapped_y = transformed_gaze[0,0,1]
			#cv2.imwrite('/Users/swetzel/Desktop/logfiles/Marie/out/'+ str(participant)+ '_' +str(cap.get(cv2.CAP_PROP_POS_FRAMES)+FRAME_OFFSET)+ '_'+ str(frame_time) +'.jpg',img2)
		else:
			print ('matrix not invertable')
	else:
		
		# if no match was found map gaze point by hand
		
		img2 = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)
		#cv2.imwrite('/Users/swetzel/Desktop/logfiles/Marie/out'+ str(participant)+ '_' + str(cap.get(cv2.CAP_PROP_POS_FRAMES)+FRAME_OFFSET)+ '_' + str(frame_time) + '.jpg',img2)
		
	return ret, frame, mapped_x, mapped_y

def write_data_to_file(data, filename, output_dir):
	
	new_log_file = open(output_dir+'/'+filename+'.txt', 'w')
	for line in data:
		for word in line:
			new_log_file.write(str(word)+'\t')
		new_log_file.write('\n')

def process_file(et_log_file, video_dir, new_log, surf, img1, kp1, des1, first_line, output_dir,  ipad_log_file_dir):
	
	current_video_file = ''
	cap = None
	task_started = False
	relative_time = 0.0;
	last_abs_time = 0.0;
	num_log = 1;
	participant = ''
	
	'''
	line = et_log_file.readline()
	line = line.rstrip('\n')
	line = line.rstrip('\r')
	split_line = line.split('	')
	line = line.split('	')
	participant = split_line[5]
	
	new_log = merge_ipad_data(participant, ipad_log_file_dir, new_log)
	'''
	for line in et_log_file:
		
		
		
		line = line.rstrip('\n')
		line = line.rstrip('\r')
		split_line = line.split('	')
		line = line.split('	')

		
		relative_time += float(split_line[12]) - last_abs_time	
	
		if ( split_line[2] != current_video_file): # stimulus changes --> load new video and ipad log file
		
			
			# write already collected data to file
			if (len(new_log) > 3):
			
				# merge with iPad data
				print ('mapping done')
				new_log = merge_ipad_data(participant, ipad_log_file_dir, new_log)
				print ('merging done')
			
			print ("load new video: " + split_line[2])
			current_video_file = split_line[2]
			
			
			path = find_video_in_folder(video_dir, current_video_file)
			task_started = False
			
			
			cap = cv2.VideoCapture(path) #open new video file
			cap.set(1,0)
			ret ,frame = cap.read()					# read first frame
			participant = split_line[5]
			print ('video loaded: ' + path) 
						
		# task start or end?
		
		if (split_line[9] == 'Text'):
			
			content = split_line[45].split('_')
			
			if (content[4] == 'TaskStart'):
				
				print(content)
				
				if (task_started == False):
					last_abs_time = float(split_line[12])
					relative_time = 0
					line.append('-') 
					line.append('-')
					line.append('-')
					line.append('-')
					line.append('-')
					line.append('-')
					line.append(str(relative_time/1000.0))
					
				task_started = True
			
			if (content[4] == 'TaskEnd'):
				
				print(content)
				
				if (task_started == True):
					line.append('-') 
					line.append('-')
					line.append('-')
					line.append('-')
					line.append('-')
					line.append('-')
					line.append(str(relative_time/1000.0))
				
				task_started = False
				

		# map visual intakes 
		if (task_started and split_line[8] == 'Eye'):
		
			
			start_time = float(split_line[12])/1000.0
			end_time = float(split_line[13])/1000.0
			
			average_time = float((start_time + end_time)/2)
			
			#ipad bounds
			minX = 0
			minY = 0
			maxX = 2048
			maxY = 1536
			
			if (split_line[9] == 'Visual Intake'):
			
			
				gaze_point_x = int(float(split_line[19]))
				gaze_point_y = int(float(split_line[20]))
			
				ret, frame, mapped_x, mapped_y = find_frame_in_video(surf, cap, ret, frame, average_time, gaze_point_x, gaze_point_y, img1, kp1, des1, participant)	# mapped gaze coordinates
		
				
				if (gaze_in_AOI(mapped_x, mapped_y, minX, minY, maxX, maxY)):
					line[37] = 'iPad'
					
				else:
					mapped_x = 0
					mapped_y = 0	
					line[37] = 'none'
				
				# append mapped coordinates to read line
				
				line.append(str(mapped_x)) 
				line.append(str(mapped_y))
				line.append('-')
				line.append('-')
				line.append('-')
				line.append('-')
				line.append(str(relative_time/1000.0))
			
				
				#print (line)
			# map saccades
			if (split_line[9] == 'Saccade'):
				
				#ipad
				minX = 0
				minY = 0
				maxX = 2048
				maxY = 1536
			
				# check start point
				#print (line)
				start_point_x = int(float(split_line[26]))
				start_point_y = int(float(split_line[27]))
				
				ret, frame, mapped_start_x, mapped_start_y = find_frame_in_video(surf, cap, ret, frame, start_time, start_point_x, start_point_y, img1, kp1, des1, participant)	# mapped gaze coordinates
				
				
				# check if on iPad
				if not (gaze_in_AOI(mapped_start_x, mapped_start_y, minX, minY, maxX, maxY)):
					
					mapped_start_x = 0
					mapped_start_y = 0	
					
				# check end point	
				end_point_x = int(float(split_line[28]))
				end_point_y = int(float(split_line[29]))	
				
				ret, frame, mapped_end_x, mapped_end_y = find_frame_in_video(surf, cap, ret, frame, end_time, end_point_x, end_point_y, img1, kp1, des1, participant)	
					
					
				if not (gaze_in_AOI(mapped_end_x, mapped_end_y, minX, minY, maxX, maxY)):
					
					mapped_end_x = 0
					mapped_end_y = 0	
					
				# append mapped coordiantes to read line
				line.append('-') 
				line.append('-')
				line.append(str(mapped_start_x))
				line.append(str(mapped_start_y))
				line.append(str(mapped_end_x))
				line.append(str(mapped_end_y))
				line.append(str(relative_time/1000.0))
				
			
		last_abs_time = float(split_line[12])	
		new_log.append(line)		
	
	if (len(new_log) > 3):
		
		print ('mapping done')
		new_log = merge_ipad_data(participant, ipad_log_file_dir, new_log)
		print ('merging done')
		write_data_to_file(new_log, str(participant), output_dir)
		print ('file ' +  str(participant)+str(num_log) + ' written')
		
	if (cap != None):	
		cap.release()
	#cv2.destroyAllWindows()
	
	
def merge_ipad_data(participant, ipad_log_file_dir, new_log):
			
	
	ipad_log_file = open(find_iPad_log(participant, ipad_log_file_dir))#open('C:/Users/Stefanie/Desktop/gaze mapping workspace/ipad log files/rIT-24_MP_L_2.csv')#find_iPad_log()
	print ('merge with iPad data ' + str(ipad_log_file.name))
	
	task_set_id = None
	task_set_started = False
	task_id = None
	first_task = True
	task_started = False
	
	for line_ipad in ipad_log_file:

		split_line_ipad = line_ipad.split(';')
		
		# 	rows in iPad log file:
		#	[0]-type	[1]-timestamp	[2]-subjectID	[3]-SetID	[4]-conditionPhysicalMental	 [5]-conditionLeftRight	 [6]-taskIndexBlock	[7]-taskIndexPresented	
		#	[8]-QRx	[9]-QRy	[10]QRz	[11]-QRw [12]-QTx	[13]-QTy	[14]-QTz	[15]-QTw	[16]-ad	[17]-dragPosX	[18]-dragPosY	[19]-wasAnsweredCorrect	
		#	[20]-taskIsSameTrial	[21]-orientationDeltaViewPortAxisX	[22]-orientationDeltaViewPortAxisY	[23]-orientationDeltaViewPortAxisZ	[24]orientationDeltaViewPortAngleDeg
		
		# old app 
		'''if split_line_ipad[0] == 'taskStart' and taskIndex == -1:
			start_time_ipad = float(split_line_ipad[1])
			# content: taskType 
			content = split_line_ipad[20]
			taskIndex = int(split_line_ipad[7])
			#print (taskIndex)
			new_log = insert_ipad_event(taskIndex,'taskStart',float(split_line_ipad[1]) - start_time_ipad, content, new_log)
		
		elif split_line_ipad[0] == 'taskEnd':
			taskIndex = -1
		elif split_line_ipad[0] == 'answer' and taskIndex != -1:
			content = split_line_ipad[19]
			new_log = insert_ipad_event(taskIndex,'answer',float(split_line_ipad[1]) - start_time_ipad, content, new_log)
		elif split_line_ipad[0] == 'dragStart' and taskIndex != -1:
			content = split_line_ipad[8]+" "+split_line_ipad[9]+" "+split_line_ipad[10]+" "+split_line_ipad[11]+" "+split_line_ipad[12]+" "+split_line_ipad[13]+" "+split_line_ipad[14]+" "+split_line_ipad[15]+" "+split_line_ipad[16]+" "+split_line_ipad[17]+" "+split_line_ipad[18] 
			new_log = insert_ipad_event(taskIndex,'dragStart',float(split_line_ipad[1]) - start_time_ipad, content, new_log)
		elif split_line_ipad[0] == 'dragUpdate' and taskIndex != -1:
			content = split_line_ipad[8]+" "+split_line_ipad[9]+" "+split_line_ipad[10]+" "+split_line_ipad[11]+" "+split_line_ipad[12]+" "+split_line_ipad[13]+" "+split_line_ipad[14]+" "+split_line_ipad[15]+" "+split_line_ipad[16]+" "+split_line_ipad[17]+" "+split_line_ipad[18] 
			new_log = insert_ipad_event(taskIndex,'dragUpdate',float(split_line_ipad[1]) - start_time_ipad, content, new_log)
		elif split_line_ipad[0] == 'dragEnd' and taskIndex != -1:
			content = split_line_ipad[8]+" "+split_line_ipad[9]+" "+split_line_ipad[10]+" "+split_line_ipad[11]+" "+split_line_ipad[12]+" "+split_line_ipad[13]+" "+split_line_ipad[14]+" "+split_line_ipad[15]+" "+split_line_ipad[16]+" "+split_line_ipad[17]+" "+split_line_ipad[18] 
			new_log = insert_ipad_event(taskIndex,'dragEnd',float(split_line_ipad[1]) - start_time_ipad, content, new_log)
		'''
		
		if split_line_ipad[3] == 'TaskSetStart' :
		
			task_set_id = split_line_ipad[8]
			task_set_start_time = float(split_line_ipad[9])
			first_task = False
			
		elif split_line_ipad[3] == 'TaskSetEnd' and task_set_started :
			
			result, new_log = insert_ipad_event(task_id,split_line_ipad[8],'TaskSetEnd',float(split_line_ipad[9]) - task_set_start_time, task_set_id, new_log)
			task_set_started = False
			first_task = False
			
		elif split_line_ipad[3] == 'TaskStart' and task_set_id != None:
			
			start_time_ipad = float(split_line_ipad[9])
			
			content = split_line_ipad[4]
			task_id = split_line_ipad[6]
			task_started = True
			
			if (first_task):
				
				result, new_log = insert_ipad_event(task_id,split_line_ipad[8],'TaskSetStart',float(split_line_ipad[9]) - start_time_ipad, task_set_id, new_log)
				if result:
					first_task = False
					task_set_started = True
					
				result, new_log = insert_ipad_event(task_id,split_line_ipad[8],'TaskStart',float(split_line_ipad[9]) - start_time_ipad, content, new_log)	
					
			elif(task_set_id == split_line_ipad[8]):
				print (task_id)
				result, new_log = insert_ipad_event(task_id,split_line_ipad[8],'TaskStart',float(split_line_ipad[9]) - start_time_ipad, content, new_log)	
		
		elif split_line_ipad[3] == 'TaskEnd' and task_set_id != None and task_started:
			task_started = False
			#print ("start " + taskIndex)
			#print (taskIndex)
			#new_log = insert_ipad_event(taskIndex,'taskStart',float(split_line_ipad[9]) - start_time_ipad, content, new_log)
		elif split_line_ipad[3] == 'DragStart' and task_id != None and task_started:
			#content = split_line_ipad[8]+" "+split_line_ipad[9]+" "+split_line_ipad[10]+" "+split_line_ipad[11]+" "+split_line_ipad[12]+" "+split_line_ipad[13]+" "+split_line_ipad[14]+" "+split_line_ipad[15]+" "+split_line_ipad[16]+" "+split_line_ipad[17]+" "+split_line_ipad[18] 
			content = split_line_ipad[4]
			result,new_log = insert_ipad_event(task_id,split_line_ipad[8],'DragStart',float(split_line_ipad[9]) - start_time_ipad, content, new_log)
				
		elif split_line_ipad[3] == 'DragUpdate' and task_id != None and task_started:
			#content = split_line_ipad[8]+" "+split_line_ipad[9]+" "+split_line_ipad[10]+" "+split_line_ipad[11]+" "+split_line_ipad[12]+" "+split_line_ipad[13]+" "+split_line_ipad[14]+" "+split_line_ipad[15]+" "+split_line_ipad[16]+" "+split_line_ipad[17]+" "+split_line_ipad[18] 
			content = split_line_ipad[4]
			result,new_log = insert_ipad_event(task_id,split_line_ipad[8],'DragUpdate',float(split_line_ipad[9]) - start_time_ipad, content, new_log)
				
		elif split_line_ipad[3] == 'ObjectRotation' and task_id != None and task_started:
			#content = split_line_ipad[8]+" "+split_line_ipad[9]+" "+split_line_ipad[10]+" "+split_line_ipad[11]+" "+split_line_ipad[12]+" "+split_line_ipad[13]+" "+split_line_ipad[14]+" "+split_line_ipad[15]+" "+split_line_ipad[16]+" "+split_line_ipad[17]+" "+split_line_ipad[18] 
			content = split_line_ipad[4]
			result,new_log = insert_ipad_event(task_id,split_line_ipad[8],'ObjectRotation',float(split_line_ipad[9]) - start_time_ipad, content, new_log)	
				
		elif split_line_ipad[3] == 'Answer' and task_id != None and task_started:
			#content = split_line_ipad[8]+" "+split_line_ipad[9]+" "+split_line_ipad[10]+" "+split_line_ipad[11]+" "+split_line_ipad[12]+" "+split_line_ipad[13]+" "+split_line_ipad[14]+" "+split_line_ipad[15]+" "+split_line_ipad[16]+" "+split_line_ipad[17]+" "+split_line_ipad[18] 
			content = split_line_ipad[4]
			result,new_log = insert_ipad_event(task_id,split_line_ipad[8],'Answer',float(split_line_ipad[9]) - start_time_ipad, content, new_log)	
		'''
		elif split_line_ipad[0] == 'taskEnd':
			taskIndex = -1
		elif split_line_ipad[0] == 'answer' and taskIndex != -1:
			content = split_line_ipad[19]
			new_log = insert_ipad_event(taskIndex,'answer',float(split_line_ipad[1]) - start_time_ipad, content, new_log)
		elif split_line_ipad[0] == 'dragStart' and taskIndex != -1:
			content = split_line_ipad[8]+" "+split_line_ipad[9]+" "+split_line_ipad[10]+" "+split_line_ipad[11]+" "+split_line_ipad[12]+" "+split_line_ipad[13]+" "+split_line_ipad[14]+" "+split_line_ipad[15]+" "+split_line_ipad[16]+" "+split_line_ipad[17]+" "+split_line_ipad[18] 
			new_log = insert_ipad_event(taskIndex,'dragStart',float(split_line_ipad[1]) - start_time_ipad, content, new_log)
		elif split_line_ipad[0] == 'dragUpdate' and taskIndex != -1:
			content = split_line_ipad[8]+" "+split_line_ipad[9]+" "+split_line_ipad[10]+" "+split_line_ipad[11]+" "+split_line_ipad[12]+" "+split_line_ipad[13]+" "+split_line_ipad[14]+" "+split_line_ipad[15]+" "+split_line_ipad[16]+" "+split_line_ipad[17]+" "+split_line_ipad[18] 
			new_log = insert_ipad_event(taskIndex,'dragUpdate',float(split_line_ipad[1]) - start_time_ipad, content, new_log)
		elif split_line_ipad[0] == 'dragEnd' and taskIndex != -1:
			content = split_line_ipad[8]+" "+split_line_ipad[9]+" "+split_line_ipad[10]+" "+split_line_ipad[11]+" "+split_line_ipad[12]+" "+split_line_ipad[13]+" "+split_line_ipad[14]+" "+split_line_ipad[15]+" "+split_line_ipad[16]+" "+split_line_ipad[17]+" "+split_line_ipad[18] 
			new_log = insert_ipad_event(taskIndex,'dragEnd',float(split_line_ipad[1]) - start_time_ipad, content, new_log)
		'''
	
	return new_log
			
	
	
def find_iPad_log(participant, ipad_dir):
	
	for subdir, dirs, files in os.walk(ipad_dir):
		
		for file in files:
			
			split_name = file.split(".")
			#print (split_name[0] + " " + participant)
			if (split_name[0] == participant):
				#print file
				return os.path.join(subdir, file)
	
def gaze_in_AOI(x, y, minX, minY, maxX, maxY):
	
	if (x >= minX and x <= maxX and y >= minY and y <= maxY):
		return True
	else:
		return False
	
	
def find_video_in_folder(video_dir, filename):
	
	for subdir, dirs, files in os.walk(video_dir):
		for subdir2, dirs2, files2 in os.walk(subdir):
			for file in files2:
				
				if (file == filename):
					
					return os.path.join(subdir2, file)
	

def insert_ipad_event(task_id,task_set_id, event, time, content, log_file):
		
	line =[]
	for i in range(0,53):
		line.append('-')
	line[8] = 'ipad'
	line[9] = event
	line[45] = content
	line[52] = time
	start = False
	current_task_id =""
	current_task_set_id =""
	
	#print ("task " + task_id)
	j =0
	for k in log_file:
	
		
		if (k[0] != 'Trial'):
			if (k[9] == 'Text'):
				
				content_split = k[45].split("_")
				
				if (content_split[4] == 'TaskStart'):
				
					# find corresponding task start in logfile
					if (event == 'TaskSetStart' ):
						
						if (content == content_split[1]):
							log_file.insert(j,line)
							return True,log_file
					
					current_task_id = content_split[2]#int(k[12])
					current_task_set_id = content_split[1]
					start = True
					


					if len(k) == 53:
						if (event == 'TaskStart' and float(k[52]) >= time and task_id == current_task_id and task_set_id == current_task_set_id):
							log_file.insert(j,line)
							return True,log_file
					#print ("current " + current_task_id)
					#print ("start of task " ,current_index)
					#print ("current" , current_index , " " , "file " , taskindex)
				
					
			#find correct timestamp for insert
			if start and len(k) == 53:
				#print (event + " " + current_task_id)
			
				#if (task_id == current_task_id):
				# print (task_id + " " + current_task_id + " " + str(time) + " "  +str(k[52]))
				if (float(k[52]) >= time and task_id == current_task_id and task_set_id == current_task_set_id and( event == 'DragStart' or event == 'DragUpdate' or event == 'ObjectRotation' or event == "Answer")):
					
					#print("insert drag")
					log_file.insert(j,line)
					
					return True,log_file
				elif (event == 'TaskSetEnd'):
					log_file.append(line)
					return True,log_file
				'''
				
				elif (task_id == current_task_id and event == 'answer' and content_split[4] == 'TASK_END'): #task end
					
					# append answer
					k[45] = k[45] + ' '+str(content)
					#print("end", content, k[46])
					return log_file
				
				elif ((taskindex) == current_index and event == 'taskStart' and content_split[4] == 'TASK_START'): #task start
				
					# append task type 
					k[46] = k[46] + ' '+str(content)
					#print("start", content, k[46])
					
					return log_file
				'''
				
					
		j += 1		
	
	return False,log_file


	
