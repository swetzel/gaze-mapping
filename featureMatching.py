# -*- coding: iso-8859-15 -*-

from functions import *
from aoi import *
import time

#if len(sys.argv)< 1: #TO DO: aoi log file
#	print 'Usage: featureMatching.py <videofile> <SMIlogfile> <ipadlogfile> <aoi file> <outputfolder>'
#	sys.exit(0)
	
start = time.time()
working_dir = '/Users/swetzel/Desktop/logfiles/Marie'
video_dir = '/Users/swetzel/Desktop/logfiles/Marie/videos'
et_log_file_dir = '/Users/swetzel/Desktop/logfiles/Marie/to_do'
ipad_log_file_dir = '/Users/swetzel/Desktop/logfiles/Marie/iPad'	
#aoi_log_file = open('C:/Users/Stefanie/Desktop/gaze mapping workspace/aois.txt')
output_dir = '/Users/swetzel/Desktop/logfiles/Marie/output'	
	
img1 = cv2.imread(working_dir+'/reference.tif',0)       # referenceImage

'''
print 'parse aoi file'
aois = []
for line in aoi_log_file:
	
	line = line.rstrip('\n')
	split_line = line.split(';')
	
	if split_line[1] == 'polygon' or split_line[1] == 'ellipse':
		
		points = []
		split_points = split_line[2].split(" ")
		
		for coords in split_points:
			coordinates = coords.split(",")
			points.append((coordinates[0],coordinates[1]))
		
		
		new_aoi = AOI(split_line[1],split_line[0],points)
		aois.append(new_aoi)
		#img1 = cv2.polylines(img1,[np.int32(new_aoi.get_points())],True,(0,255,0))
'''		

#print(et_log_file_dir)

for subdir, dirs, files in os.walk(et_log_file_dir):
	
	for file in files:

		if not(file.startswith(".")):
		
		
			# Initiate SURF detector
			surf = cv2.xfeatures2d.SURF_create()
			# find the keypoints and descriptors with SURF
			kp1, des1 = surf.detectAndCompute(img1,None)

			# new et log file
			list_of_logged_lines = []
		
		
			et_log_file = open(subdir + os.sep + file, 'r')
			first_line = et_log_file.readline()	# ignore first line
			first_line = et_log_file.readline()	# read first line and append new variables
			first_line = first_line.rstrip('\n')
			first_line = first_line.rstrip('\r')
			first_line = first_line.split('	')
			first_line.append('Visual Intake Mapped Position X')
			first_line.append('Visual Intake Mapped Position Y')
			first_line.append('Saccade Mapped Start Position X')
			first_line.append('Saccade Mapped Start Position Y')
			first_line.append('Saccade Mapped End Position X')
			first_line.append('Saccade Mapped End Position Y')
			first_line.append('Relative Task Time')
			
			list_of_logged_lines.append(first_line)	
			
			# process et log file
			print ('process file: ' + file)
	
			process_file(et_log_file, video_dir, list_of_logged_lines, surf, img1, kp1, des1, first_line, output_dir, ipad_log_file_dir)
			
			# TEST
			#print find_iPad_log(ipad_log_file_dir, "rIT-04_MP_L_5")
			#save_frames('F:/rotateIt_Veronika/rIT-24_MP_L-[403162ef-4d29-4a9c-82cf-90a9b1319786]/rIT-24_MP_L-2-recording.avi')
		

'''

# parse log file
task_started = False
relative_time = 0.0;
last_abs_time = 0.0;
print 'reading eye tracking log file'
for line in et_log_file:
	
	split_line = line.split('	')

	# 	rows in SMI log file:	
	#	[0]-Trial	[1]-Trial Start Raw Time [ms]	[2]-Stimulus	[3]-Export Start Trial Time [ms]	 [4]-Export End Trial Time [ms]	[5]-Participant	
	#	[6]-Color [7]-Group	[8]-Tracking Ratio [%]	[9]-Category Group	[10]-Category	[11]-Eye [12]-Index	[13]-Event Start Trial Time [ms]	[14]-Event End Trial Time [ms]	
	#	[15]-Event Start Raw Time [ms]	[16]-Event End Raw Time [ms]	[17]Event Duration [ms]	 [18]-Event Start Video Time [ms]	[19]-Event End Video Time [ms]	
	# 	[20]-Visual Intake Position X [px]	[21]-Visual Intake Position Y [px]	[22]-Visual Intake Average Pupil Size X [px]	[23]-Visual Intake Average Pupil Size Y [px]	
	#	[24]-Visual Intake Average Pupil Diameter [mm]	[25]-Visual Intake Dispersion X [px]	[26]-Visual Intake Dispersion Y [px]	[27]-Saccade Start Position X [px]	
	#	[28]-Saccade Start Position Y [px]	[29]-Saccade End Position X [px]	[30]-Saccade End Position Y [px]	[31]-Saccade Amplitude [°]	[32]Saccade Acceleration Average [°/s²]	
	#	[33]-Saccade Acceleration Peak [°/s²]	[34]-Saccade Deceleration Peak [°/s²]	[35]-Saccade Velocity Average [°/s]	 [36]-Saccade Velocity Peak [°/s]	[37]-Saccade Peak Velocity at [%]	
	#	[38]-AOI Name	[39]-Annotation Name	[40]-Annotation Description	 [41]-Annotation Tags	[42]-Mouse Position X [px]	[43]-Mouse Position Y [px]	[44]-Scroll Direction X	 
	#	[45]-Scroll Direction Y	[46]-Content
	
	#	new content:
	#	[47]-Visual Intake Mapped Position X  [48]-Visual Intake Mapped Position Y  [49]-Saccade Mapped Start Position X  [50]-Saccade Mapped Start Position Y
	#	[51]-Saccade Mapped End Position X  [52]-Saccade Mapped End Position Y [53]-Relative Task Time
		
	line = line.rstrip('\n')
	line = line.split('	')

	relative_time += float(split_line[13]) - last_abs_time	
	
	# task start or end?
	if (split_line[10] == 'Text'):
		
		content = split_line[46].split(' ')
		
		if (content[4] == 'TASK_START\n'):
			
			task_started = True
			last_abs_time = float(split_line[13])
			relative_time = 0
			line.append('-') 
			line.append('-')
			line.append('-')
			line.append('-')
			line.append('-')
			line.append('-')
			line.append(str(relative_time/1000.0))
		
		if (content[4] == 'TASK_END'):
			task_started = False
			line.append('-') 
			line.append('-')
			line.append('-')
			line.append('-')
			line.append('-')
			line.append('-')
			line.append(str(relative_time/1000.0))
			
					
	
			
	# map visual intakes 
	if (split_line[10] == 'Visual Intake' and task_started):
	
		# start time
		start_time = split_line[18].split(':')
		start_milliseconds = float(start_time[3])/1000.0
		start_seconds = float(start_time[2])
		start_minutes = float(start_time[1])/60.0
		start_hours = float(start_time[0])/3600.0
		
		# end time
		end_time = split_line[19].split(':')
		end_milliseconds = float(end_time[3])/1000.0
		end_seconds = float(end_time[2])
		end_minutes = float(end_time[1])/60.0
		end_hours = float(end_time[0])/3600.0
		
		average_time = (start_hours + start_minutes + start_seconds + start_milliseconds + end_hours + end_minutes + end_seconds + end_milliseconds)/2.0
		
		gaze_point_x = int(float(split_line[20]))
		gaze_point_y = int(float(split_line[21]))
		
		
		ret, frame, frame_num, mapped_x, mapped_y = find_frame_in_video(surf, cap, frame_num, ret, frame, average_time, gaze_point_x, gaze_point_y, img1, kp1, des1, sys.argv[5])	# mapped gaze coordinates
		#mapped_x = 0
		#mapped_y = 0
	
		# TO DO: check witch aois
		for aoi in aois:
			if aoi.contains_gaze(mapped_x,mapped_y):
				line[38] = aoi.get_name()
			else:
				line[38] = 'none'
		
		# append mapped coordiantes to read line
		line.append(str(mapped_x)) 
		line.append(str(mapped_y))
		line.append('-')
		line.append('-')
		line.append('-')
		line.append('-')
		line.append(str(relative_time/1000.0))
		
	last_abs_time = float(split_line[13])	
	list_of_logged_lines.append(line)		

# merge with ipad logfile	
answer = 0
taskIndex = 1
taskIndexPresented = 0
relative_time = 0.0;

print 'read ipad log file'
for line in ipad_log_file:
	
	split_line = line.split(';')
	
	# 	rows in iPad log file:
	#	[0]-type	[1]-timestamp	[2]-subjectID	[3]-SetID	[4]-conditionPhysicalMental	 [5]-conditionLeftRight	 [6]-taskIndexBlock	[7]-taskIndexPresented	
	#	[8]-QRx	[9]-QRy	[10]QRz	[11]-QRw [12]-QTx	[13]-QTy	[14]-QTz	[15]-QTw	[16]-ad	[17]-dragPosX	[18]-dragPosY	[19]-wasAnsweredCorrect	
	#	[20]-taskIsSameTrial	[21]-orientationDeltaViewPortAxisX	[22]-orientationDeltaViewPortAxisY	[23]-orientationDeltaViewPortAxisZ	[24]orientationDeltaViewPortAngleDeg
	
	
	if split_line[0] == 'taskStart':
		start_time = float(split_line[1])
		# content: taskType 
		content = split_line[20]
		list_of_logged_lines = insert_ipad_event(taskIndex,'taskStart',float(split_line[1]) - start_time, content, list_of_logged_lines)
	
	elif split_line[0] == 'taskEnd':
		taskIndex += 1
	elif split_line[0] == 'answer':
		content = split_line[19]
		list_of_logged_lines = insert_ipad_event(taskIndex,'answer',float(split_line[1]) - start_time, content, list_of_logged_lines)
	elif split_line[0] == 'dragStart':
		# content
		content = split_line[8]+" "+split_line[9]+" "+split_line[10]+" "+split_line[11]+" "+split_line[12]+" "+split_line[13]+" "+split_line[14]+" "+split_line[15]+" "+split_line[16]+" "+split_line[17]+" "+split_line[18] 
		list_of_logged_lines = insert_ipad_event(taskIndex,'dragStart',float(split_line[1]) - start_time, content, list_of_logged_lines)
	elif split_line[0] == 'dragUpdate':
		content = split_line[8]+" "+split_line[9]+" "+split_line[10]+" "+split_line[11]+" "+split_line[12]+" "+split_line[13]+" "+split_line[14]+" "+split_line[15]+" "+split_line[16]+" "+split_line[17]+" "+split_line[18] 
		list_of_logged_lines = insert_ipad_event(taskIndex,'dragUpdate',float(split_line[1]) - start_time, content, list_of_logged_lines)
	elif split_line[0] == 'dragEnd':
		content = split_line[8]+" "+split_line[9]+" "+split_line[10]+" "+split_line[11]+" "+split_line[12]+" "+split_line[13]+" "+split_line[14]+" "+split_line[15]+" "+split_line[16]+" "+split_line[17]+" "+split_line[18] 
		list_of_logged_lines = insert_ipad_event(taskIndex,'dragEnd',float(split_line[1]) - start_time, content, list_of_logged_lines)

	
# write new log file
for line in list_of_logged_lines:
	for word in line:
		new_log_file.write(str(word)+'\t')
	new_log_file.write('\n')
'''
end = time.time()
print ('done' +str((end - start)/60))
