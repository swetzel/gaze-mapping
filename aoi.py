'''
Created on 28.06.2016

@author: Stefanie
'''
import matplotlib.path as mplPath



class AOI(object):
    
    aoi_type = None
    name = None
    points = []
    radius_x = 0
    radius_y = 0

    def __init__(self,type,aoi_name, aoi_points):
       
        self.aoi_type = type
        self.name = aoi_name
        
        if type == 'polygon' or type == 'rectangle':
            self.points = aoi_points
    
        elif type == 'ellipse':
            self.points.append(aoi_points[0])
            self.radius_x = aoi_points[1][0]
            self.radius_y = aoi_points[1][1]
    
        self.contains_gaze(150, 75)
        
    def contains_gaze(self, gaze_point_x, gaze_point_y):
        
        if self.aoi_type == 'polygon':
                           
            path = mplPath.Path(self.points)
            return path.contains_point((gaze_point_x,gaze_point_y))
            
        elif type == 'ellipse':
          
            # to do
            pass
        elif type == 'rectangle':
            
            pass 
            
        else:
            return False   
        
    def get_points(self):
        return self.points
    
    def get_name(self):
        return self.name