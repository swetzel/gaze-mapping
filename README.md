This Python script maps fixation positions based on the video stream of a SMI Glasses 2.0 eye tracker to a reference image. Based on corresponding points from the SIFT algorithm the position in the reference image is determined by computing a homography.
![132.0_5.5_rIT-01_PM_L_1.jpg](https://bitbucket.org/repo/pB9gkG/images/80641107-132.0_5.5_rIT-01_PM_L_1.jpg)

Contact: stefanie.wetzel@gmail.com